package me.randomhashtags.randompackage.dev;

public enum RaidEventPhaseTrigger {
    BEACON_DESTROYED,
    LOOTBAG_CLAIMED
}
