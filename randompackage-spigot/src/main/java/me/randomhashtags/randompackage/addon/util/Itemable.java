package me.randomhashtags.randompackage.addon.util;

import org.bukkit.inventory.ItemStack;

public interface Itemable extends Identifiable {
    ItemStack getItem();
}
