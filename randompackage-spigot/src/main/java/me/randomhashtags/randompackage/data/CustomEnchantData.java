package me.randomhashtags.randompackage.data;

import me.randomhashtags.randompackage.addon.living.LivingCustomEnchantEntity;

import java.util.List;

public interface CustomEnchantData {
    List<LivingCustomEnchantEntity> getEntities();
}
