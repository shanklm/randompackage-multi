package me.randomhashtags.randompackage.attribute;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public interface Listable {
    HashMap<UUID, List<String>> LIST = new HashMap<>();
}
